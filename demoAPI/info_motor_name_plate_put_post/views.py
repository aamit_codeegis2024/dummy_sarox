from .models import GetInfoMot
from .serializer import GetInfoMotSer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def Get_mot_info_add(request, equip_type=None):
    try:
        if request.method == 'GET':
            if equip_type is None:
                motors = GetInfoMot.objects.all()
                serializer = GetInfoMotSer(motors, many=True)
            else:
                motor = GetInfoMot.objects.get(equip_type=equip_type)
                serializer = GetInfoMotSer(motor)
            
            return Response(serializer.data, status=status.HTTP_200_OK)

        elif request.method == 'POST':
            serializer = GetInfoMotSer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'PUT':
            if equip_type is None:
                return Response({'error': 'Equip type is required for update'}, status=status.HTTP_400_BAD_REQUEST)

            motor = GetInfoMot.objects.get(equip_type=equip_type)
            serializer = GetInfoMotSer(motor, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'DELETE':
            if equip_type is None:
                return Response({'error': 'Equip type is required for delete'}, status=status.HTTP_400_BAD_REQUEST)

            motor = GetInfoMot.objects.get(equip_type=equip_type)
            motor.delete()
            return Response({'message': 'Motor deleted successfully'}, status=status.HTTP_204_NO_CONTENT)

    except GetInfoMot.DoesNotExist:
        return Response({'error': 'Motor not found'}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response({'error': f'Internal Server Error: {e}'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
