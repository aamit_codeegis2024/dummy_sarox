from django.apps import AppConfig


class InfoMotorNamePlatePutPostConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "info_motor_name_plate_put_post"
