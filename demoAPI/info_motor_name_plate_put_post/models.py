from django.db import models

class GetInfoMot(models.Model):
    equip_type = models.IntegerField(primary_key=True)
    speed_rpm = models.DecimalField(max_digits=20, decimal_places=10)
    cos_fi = models.DecimalField(max_digits=20, decimal_places=10)
    power = models.DecimalField(max_digits=20, decimal_places=10)
    stator_winding_conn = models.IntegerField()
    rotor_vol = models.DecimalField(max_digits=20, decimal_places=10)
    efficiency = models.DecimalField(max_digits=20, decimal_places=10)
    line_vol = models.DecimalField(max_digits=20, decimal_places=10)
    nom_current = models.DecimalField(max_digits=20, decimal_places=10)
    rotor_winding_conn = models.IntegerField()
    rotor_current = models.DecimalField(max_digits=20, decimal_places=10)
    insulation_class = models.IntegerField()
    remarks = models.CharField(max_length=255)  # Use CharField if it's appropriate for your data

    def __str__(self):
        return self.equip_type
