from info_motor_name_plate_put_post.models import GetInfoMot

from rest_framework.serializers import ModelSerializer

class GetInfoMotSer(ModelSerializer):
    class Meta:
        model=GetInfoMot
        fields=('equip_type','speed_rpm','cos_fi','power','stator_winding_conn','rotor_vol','efficiency','line_vol','nom_current','rotor_winding_conn',
                'rotor_current','insulation_class','remarks')