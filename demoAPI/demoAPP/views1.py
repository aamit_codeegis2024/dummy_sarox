# from django.shortcuts import render

# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.parsers import JSONParser
# from django.http.response import JsonResponse
# from django.http import HttpResponse
# from demoAPP.models import Student,Employee
# from demoAPP.serializer import StudentSerializer,EmployeeSerializer
# # Create your views here.
# # def get_data(request):
# #     return render(request=)

# #now write code for API
# @csrf_exempt
# def student_db(request,id=0):
#     if request.method=='GET':
#         students=Student.objects.all()
#         stu_serializer=StudentSerializer(students,many=True)
        
#         return JsonResponse(stu_serializer.data,safe=False)
    
#     elif request.method=='POST':
#         stu_data=JSONParser().parse(request)
#         stu_serializer=StudentSerializer(data=stu_data)
#         if stu_serializer.is_valid():
#             stu_serializer.save()
#             return JsonResponse("ADD Successfully")
        
#         return JsonResponse("Failed to add ",safe=False)
    
#     elif request.method=='PUT':
#         stu_data=JSONParser().parse(request)
#         stu_get=Student.objects.get(std_id=stu_data['std_id'])
#         stu_serializer=StudentSerializer(Student,data=stu_data)
        
#         if stu_serializer.is_valid():
#             stu_serializer.save()
#             return JsonResponse("Update Successfully")
#         return JsonResponse('Update go failed')
    
#     elif request.method=='DELETE':
#         stu_del=Student.objects.get(std_id=id)
#         stu_del.delete()
#         return JsonResponse('Delete Successfully')
        
    

    
# from django.shortcuts import render
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.parsers import JSONParser
# from django.http import JsonResponse
# from demoAPP.models import Student
# from demoAPP.serializer import StudentSerializer

# # Create your views here.

# @csrf_exempt
# def student_db(request, id=0):
#     if request.method == 'GET':
#         students = Student.objects.all()
#         stu_serializer = StudentSerializer(students, many=True)
#         return JsonResponse(stu_serializer.data, safe=False)
    
#     elif request.method == 'POST':
#         stu_data = JSONParser().parse(request)
#         stu_serializer = StudentSerializer(data=stu_data)
#         if stu_serializer.is_valid():
#             stu_serializer.save()
#             return JsonResponse("Added Successfully", safe=False)
#         return JsonResponse("Failed to add", safe=False)
    
#     elif request.method == 'PUT':
#         stu_data = JSONParser().parse(request)
#         stu_instance = Student.objects.get(std_id=stu_data['std_id'])
#         stu_serializer = StudentSerializer(stu_instance, data=stu_data)
#         if stu_serializer.is_valid():
#             stu_serializer.save()
#             return JsonResponse("Updated Successfully", safe=False)
#         return JsonResponse('Update failed', safe=False)
    
#     elif request.method == 'DELETE':
#         try:
#             stu_instance = Student.objects.get(std_id=id)
#             stu_instance.delete()
#             return JsonResponse('Deleted Successfully', safe=False)
#         except Student.DoesNotExist:
#             return JsonResponse('Student not found', status=404, safe=False)
    
#     return JsonResponse('Invalid request', safe=False)

        
        
# from django.shortcuts import render
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.parsers import JSONParser
# from django.http import JsonResponse
# from demoAPP.models import Student
# from demoAPP.serializer import StudentSerializer

# # Create your views here.

# @csrf_exempt
# def student_db(request, id=0):
#     if request.method == 'GET':
#         students = Student.objects.all()
#         stu_serializer = StudentSerializer(students, many=True)
#         return JsonResponse(stu_serializer.data, safe=False)
    
#     elif request.method == 'POST':
#         stu_data = JSONParser().parse(request)
#         stu_serializer = StudentSerializer(data=stu_data)
#         if stu_serializer.is_valid():
#             stu_serializer.save()
#             return JsonResponse({'message': 'Added Successfully'}, status=201)
#         return JsonResponse(stu_serializer.errors, status=400)
    
#     elif request.method == 'PUT':
#         stu_data = JSONParser().parse(request)
#         try:
#             stu_instance = Student.objects.get(std_id=stu_data['std_id'])
#         except Student.DoesNotExist:
#             return JsonResponse({'error': 'Student not found'}, status=404)
        
#         stu_serializer = StudentSerializer(stu_instance, data=stu_data)
#         if stu_serializer.is_valid():
#             stu_serializer.save()
#             return JsonResponse({'message': 'Updated Successfully'})
#         return JsonResponse(stu_serializer.errors, status=400)
    
#     elif request.method == 'DELETE':
#         try:
#             stu_instance = Student.objects.get(std_id=id)
#             stu_instance.delete()
#             return JsonResponse({'message': 'Deleted Successfully'})
#         except Student.DoesNotExist:
#             return JsonResponse({'error': 'Student not found'}, status=404)
    
#     return JsonResponse({'error': 'Invalid request'}, status=400)




from django.shortcuts import render,get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from django.http import Http404

from demoAPP.models import Student,Employee,MotorList
from demoAPP.serializer import StudentSerializer,MotorSerializer,EmployeeSerializer

@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def student_db(request, pk=None):
    if request.method == 'GET':
        if pk:
            student = get_object_or_404(Student, pk=pk)
            serializer = StudentSerializer(student)
        else:
            students = Student.objects.all()
            serializer = StudentSerializer(students, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PUT':
        student = get_object_or_404(Student, pk=pk)
        serializer = StudentSerializer(student, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        student = get_object_or_404(Student, pk=pk)
        student.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# for Motor


# @api_view(['GET'])
# def motor_db(request, pk=None):
#     if request.method == 'GET':
#         if pk:
#             motor = get_object_or_404(MotorList, pk=pk)
#             serializer = MotorSerializer(motor)
#         else:
#             motors = MotorList.objects.all()
#             serializer = MotorSerializer(motors, many=True)
#         return Response(serializer.data)

# def motor_db(request, pk=None):
#     if request.method == 'GET':
#         if pk:
#             motor= get_object_or_404(MotorList, pk=pk)
#             serializer = MotorSerializer(motor)
#         else:
#             motors= MotorList.objects.all()
#             serializer = MotorSerializer(motors, many=True)
#         return Response(serializer.data)