from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from demoAPP.models import MotorList
from demoAPP.serializer import MotorListSerializer

@api_view(['GET'])
def motor_db(request, id=0):
    try:
        if request.method == 'GET':
            if id == 0:
                # Retrieve all motors if no id is provided
                motors = MotorList.objects.all()
                serializer = MotorListSerializer(motors, many=True)
            else:
                # Retrieve a specific motor if id is provided
                motor = MotorList.objects.get(id=id)
                serializer = MotorListSerializer(motor)

            return Response(serializer.data, status=status.HTTP_200_OK)

    except MotorList.DoesNotExist:
        return Response({'error': 'Motor not found'}, status=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response({'error': f'Internal Server Error: {e}'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
