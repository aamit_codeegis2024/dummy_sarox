from rest_framework import serializers
from demoAPP.models import Employee,Student,MotorList


# make class for serializer

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model=Student  # here we put database name where we provide in model.py
        fields=('std_id','std_anme','std_state')   # here we pass columns name which has we given in database.


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Employee
        fields=('emp_id','emp_anme','emp_state')
        
        
class MotorListSerializer(serializers.ModelSerializer):
    class Meta:
        model=MotorList
        fields=('s_no','equipment_name','equipment_type','place_of_installations','utility_tag','serial_no','motor_no','actions')