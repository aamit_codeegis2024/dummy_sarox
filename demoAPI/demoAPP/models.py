from django.db import models

# Create your models here.
class Student(models.Model):
    std_id=models.AutoField(primary_key=True)
    std_anme=models.CharField()
    std_state=models.CharField()
    
    
class Employee(models.Model):
    emp_id=models.AutoField(primary_key=True)
    emp_anme=models.CharField()
    emp_state=models.CharField()
    
class MotorList(models.Model):
    s_no = models.AutoField(primary_key=True)
    equipment_name = models.CharField(max_length=255)
    equipment_type = models.CharField(max_length=255)
    place_of_installations = models.CharField(max_length=255)
    utility_tag = models.CharField(max_length=255)  # Corrected field name
    serial_no = models.DecimalField(max_digits=20, decimal_places=10)
    motor_no = models.DecimalField(max_digits=20, decimal_places=10)
    actions = models.TextField()
    